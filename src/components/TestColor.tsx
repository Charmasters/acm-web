export default function TestColor() {
    return (
        <div className="grid grid-cols-6 h-32">
            <div className="bg-blue" />
            <div className="bg-gold" />
            <div className="bg-grey" />
            <div className="bg-orange" />
            <div className="bg-lightblue" />
            <div className="bg-darkblue" />
        </div>
    )
}
